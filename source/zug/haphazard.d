module zug.haphazard;

alias Randy = int delegate();
Randy BBS(int seed, int first_prime = 23773, int second_prime = 19069)
{
    auto modulo = first_prime * second_prime;
    int last_rand = seed;
    Randy generator = delegate() {
        auto next = (last_rand * last_rand) % modulo;
        last_rand = next;
        return next;
    };

    return generator;

}

unittest
{
    import std.stdio;

    auto generator = BBS(1243134);
    assert(typeof(&generator).stringof == "int delegate()*");
    assert(-352757267 == generator());
    assert(26277806 == generator());
    assert(-278840764 == generator());
}
